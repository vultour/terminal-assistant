#!/usr/bin/env python3
import npyscreen as nps

from windows import WelcomeWindow, PasswordWindow


class TerminalAssistant(nps.NPSAppManaged):
    def onStart(self):
        self.addForm("MAIN", WelcomeWindow)
        self.addForm("PASSWORD", PasswordWindow)


if __name__ == "__main__":
    assistant = TerminalAssistant()
    assistant.run()
