import npyscreen as nps

from .base_window import BaseWindow


class WelcomeWindow(BaseWindow):
    def create_cont(self):
        self.add(
            nps.MultiLineEdit,
            value="""
                Welcome to Terminal Assistant!
                Please use the menu to get started.
            """,
            max_height=5,
            editable=False
        )
        self.add(nps.MultiLine, values=[""])
