import npyscreen as nps


class BaseWindow(nps.FormBaseNewWithMenus):
    def create(self):
        self.name = "Terminal Assistant"
        menu = self.new_menu("Menu")
        menu.addItem(
            text="Password Generator",
            onSelect=self.switch_form,
            keywords={"form": "PASSWORD"}
        )
        menu.addItem(
            text="Exit",
            onSelect=self.trigger_exit
        )
        self.create_cont()

    def create_cont(self):
        pass

    def switch_form(self, **kwargs):
        self.parentApp.switchForm(kwargs["form"])

    def trigger_exit(self):
        self.parentApp.switchForm(None)
