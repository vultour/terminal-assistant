import npyscreen as nps
import random

from .base_window import BaseWindow


class PasswordError(Exception):
    pass


class PasswordGenerator:
    alphabets = {
        "characters": "abcdefghijklmnopqrstuvxyz",
        "numbers": "0123456789",
        "symbols": """,./;'"\\[]=-!@#$%^&*()_+{}|?><""",
    }

    def generate_password(
        self,
        length=64,
        lowercase=True,
        uppercase=True,
        numbers=True,
        symbols=True,
        unique=True
    ):
        character_set = []
        if lowercase:
            character_set.extend(self.alphabets["characters"])
        if uppercase:
            character_set.extend(self.alphabets["characters"].upper())
        if numbers:
            character_set.extend(self.alphabets["numbers"])
        if symbols:
            character_set.extend(self.alphabets["symbols"])

        result = ""
        for i in range(length):
            if len(character_set) < 1:
                result += self.generate_password(
                    length - i,
                    lowercase, uppercase, numbers, symbols, unique
                )
                break
            character = random.choice(character_set)
            result += character
            if unique:
                character_set.remove(character)

        return result


class PasswordWindow(BaseWindow):
    generator = PasswordGenerator()

    def create_cont(self):
        self.passwords = self.add(
            nps.MultiLine,
            values=[""],
            max_height=33,
            editable=False
        )
        self.add(
            nps.ButtonPress,
            name="Simple",
            max_height=3,
            when_pressed_function=self.generate_simple,
        )
        self.add(
            nps.ButtonPress,
            name="Complex",
            when_pressed_function=self.generate_complex,
        )
        self.generate_simple()

    def generate_simple(self):
        self.generate_passwords(mode="simple")

    def generate_complex(self):
        self.generate_passwords(mode="complex")

    def generate_passwords(self, **kwargs):
        mode = kwargs["mode"]
        self.passwords.values = []
        for i in range(32):
            password = None
            if mode == "simple":
                password = self.generator.generate_password(
                    length=64, lowercase=True, uppercase=True, numbers=True,
                    symbols=False, unique=True
                )
            elif mode == "complex":
                password = self.generator.generate_password()
            self.passwords.values.append(password)
        self.display()
